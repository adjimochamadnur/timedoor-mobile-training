package net.timedoor.newandroidtraining.presenter.talk

import io.reactivex.Observable
import net.timedoor.newandroidtraining.data.model.talk.SendMessageData
import net.timedoor.newandroidtraining.data.model.talk.TalkData
import net.timedoor.newandroidtraining.data.model.talk.TalkItem
import net.timedoor.newandroidtraining.presenter.common.BaseContract
import retrofit2.http.*

interface TalkContract {
    interface View : BaseContract.View {
        fun talkFetched(data: List<TalkItem>)
        fun sendMessageSuccess(data: SendMessageData)
    }

    interface Presenter {
        fun fetchTalk(borderMessageId: Int)
        fun sendMessage(message: String)
        fun sendMessage(id: Int, isMediaImage: Boolean)
    }

    interface Service {
        @GET("{ctrl}/{action}")
        fun fetchTalk(
            @Path("ctrl") ctrl: String,
            @Path("action") action: String,
            @QueryMap hashMap: HashMap<String, Any>
        ) : Observable<TalkData>

        @FormUrlEncoded
        @POST("{ctrl}/{action}")
        fun sendMessage(
            @Path("ctrl") ctrl: String,
            @Path("action") action: String,
            @FieldMap hashMap: HashMap<String, Any>
        ) : Observable<SendMessageData>
    }
}
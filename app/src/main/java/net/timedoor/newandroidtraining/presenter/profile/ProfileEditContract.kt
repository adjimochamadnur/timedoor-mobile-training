package net.timedoor.newandroidtraining.presenter.profile

import io.reactivex.Observable
import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.data.model.profile.ProfileEditData
import net.timedoor.newandroidtraining.presenter.common.BaseContract
import retrofit2.http.*

interface ProfileEditContract {
    interface View : BaseContract.View {
        fun successEditProfile(data: ProfileEditData)
    }

    interface Presenter {
        fun editProfile(
            nickName: String? = null,
            birthday: String? = null,
            residence: String? = null,
            gender: Int = 0,
            job: Int = 0,
            personality: Int = 0,
            hobby: String? = null,
            aboutMe: String? = null,
            imageId: Int = 0,
            languange: String? = null
        )
    }

    interface Service {
        @FormUrlEncoded
        @POST("{ctrl}/{action}")
        fun editProfile(
            @Path("ctrl") ctrl: String,
            @Path("action") action: String,
            @Query(Constants.REQUEST_NAME_ACCESS_TOKEN) accessToken: String,
            @FieldMap hashMap: HashMap<String, Any>
        ) : Observable<ProfileEditData>
    }
}
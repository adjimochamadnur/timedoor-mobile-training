package net.timedoor.newandroidtraining.presenter.register

import android.security.identity.ResultData

interface RegisterContract {
    interface View {
        fun registerSuccess(data: ResultData)
    }

    interface Presenter {
        fun register(username: String, email: String, password: String, language: String)
    }

    interface Service {
    }
}
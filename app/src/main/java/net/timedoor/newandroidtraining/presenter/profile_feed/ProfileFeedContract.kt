package net.timedoor.newandroidtraining.presenter.profile_feed

import io.reactivex.Observable
import net.timedoor.newandroidtraining.data.model.profile_feed.ProfileFeedData
import net.timedoor.newandroidtraining.data.model.profile_feed.ProfileFeedItem
import net.timedoor.newandroidtraining.presenter.common.BaseContract
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap
import kotlin.collections.HashMap

interface ProfileFeedContract {
    interface View : BaseContract.View {
        fun feedFetched(data: List<ProfileFeedItem>)
    }

    interface Presenter {
        fun fetchProfileFeed()
    }

    interface Service {
        @GET("{ctrl}/{action}")
        fun fetchProfileFeed(
            @Path("ctrl") ctrl: String,
            @Path("action") action: String,
            @QueryMap(encoded = true) hashMap: HashMap<String, String>
        ) : Observable<ProfileFeedData>
    }
}
package net.timedoor.newandroidtraining.presenter.media

interface MediaUploadContract {
    interface View {
        fun uploadImageSuccess()
        fun uploadVideoSuccess()
    }

    interface Presenter {
        fun uploadImage(location: String, data: String)
        fun uploadVideo(data: String)
    }

    interface Service {
        // TODO() Create service for upload media
    }
}
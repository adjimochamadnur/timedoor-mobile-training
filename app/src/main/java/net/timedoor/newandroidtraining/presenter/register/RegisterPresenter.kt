package net.timedoor.newandroidtraining.presenter.register

import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.data.handler.register.RegisterHandler
import net.timedoor.newandroidtraining.data.model.register.RegisterData

class RegisterPresenter(private val view: RegisterContract.View) : RegisterContract.Presenter {
    private val handler = RegisterHandler()

    override fun register(username: String, email: String, password: String, language: String) {
        val hashMap = hashMapOf<String, String>()
        hashMap[Constants.REQUEST_NAME_NICKNAME] = username
        hashMap[Constants.REQUEST_NAME_LOGIN_ID] = email
        hashMap[Constants.REQUEST_NAME_PASSWORD] = password
        hashMap[Constants.REQUEST_NAME_LANGUAGE] = language

        // TODO() handle sent and received data from server
    }

    private fun saveData(data: RegisterData) {

    }
}
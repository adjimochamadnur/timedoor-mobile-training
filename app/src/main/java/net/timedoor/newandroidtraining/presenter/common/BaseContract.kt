package net.timedoor.newandroidtraining.presenter.common

interface BaseContract {
    interface View {
        fun showError(title: String, message: String)
    }
}
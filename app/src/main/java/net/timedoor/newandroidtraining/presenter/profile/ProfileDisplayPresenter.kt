package net.timedoor.newandroidtraining.presenter.profile

import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.data.handler.profile.ProfileDisplayHandler

class ProfileDisplayPresenter(private val view: ProfileDisplayContract.View) : ProfileDisplayContract.Presenter {
    private val handler = ProfileDisplayHandler()
    private val accessToken: String
    private val userId: Int

    init {
        // TODO() prepare user access token and userId
        accessToken = ""
        userId = 0
    }

    override fun fetchProfileDisplay() {
        getQueryMap()
    }

    override fun fetchProfileDisplay(userId: Int) {
        getQueryMap()
    }

    private fun getQueryMap() : HashMap<String, Any> {
        // TODO() create query map to send to API
        return hashMapOf()
    }
}
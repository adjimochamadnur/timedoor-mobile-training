package net.timedoor.newandroidtraining.presenter.talk

import net.timedoor.newandroidtraining.data.handler.talk.TalkListHandler

class TalkListPresenter(private val view: TalkListContract.View) : TalkListContract.Presenter {
    private val handler = TalkListHandler()
    private val accessToken: String

    init {
        accessToken = ""
    }

    override fun fetchTalk() {
        TODO("Not yet implemented")
    }

    override fun deleteTalk(talkIds: String) {
        TODO("Not yet implemented")
    }
}
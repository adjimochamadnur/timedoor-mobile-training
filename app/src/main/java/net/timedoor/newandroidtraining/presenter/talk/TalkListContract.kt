package net.timedoor.newandroidtraining.presenter.talk

import io.reactivex.Observable
import net.timedoor.newandroidtraining.data.model.talk.DeleteTalkData
import net.timedoor.newandroidtraining.data.model.talk.TalkListData
import net.timedoor.newandroidtraining.presenter.common.BaseContract
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface TalkListContract {
    interface View : BaseContract.View {
        fun talkListFetched(data: TalkListData)
        fun deleteSuccess(data: DeleteTalkData, talkIds: String)
    }

    interface Presenter {
        fun fetchTalk()
        fun deleteTalk(talkIds: String)
    }

    interface Service {
        @GET("{ctrl}/{action}")
        fun fetchTalkList(
            @Path("ctrl") ctrl: String,
            @Path("action") action: String,
            @QueryMap hashMap: HashMap<String, String>
        ) : Observable<TalkListData>

        @GET("{ctrl}/{action}")
        fun deleteTalks(
            @Path("ctrl") ctrl: String,
            @Path("action") action: String,
            @QueryMap hashMap: HashMap<String, String>
        ) : Observable<DeleteTalkData>
    }
}
package net.timedoor.newandroidtraining.presenter.login

import net.timedoor.newandroidtraining.data.model.login.LoginData
import net.timedoor.newandroidtraining.presenter.common.BaseContract
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface LoginContract {
    interface View : BaseContract.View {
        fun loginResponse(data: LoginData)
    }

    interface Presenter {
        fun login(email: String, password: String, language: String)
    }

    interface Service {
        @GET("{ctrl}/{action}")
        fun login(
            @Path("ctrl") ctrl: String,
            @Path("action") action: String,
            @QueryMap loginQuery: HashMap<String, String>
        ) : Observable<LoginData>
    }
}
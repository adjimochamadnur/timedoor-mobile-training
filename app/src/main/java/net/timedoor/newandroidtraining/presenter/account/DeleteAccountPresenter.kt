package net.timedoor.newandroidtraining.presenter.account

import net.timedoor.newandroidtraining.common.doSubscribe
import net.timedoor.newandroidtraining.data.handler.account.DeleteAccountHandler
import net.timedoor.newandroidtraining.data.handler.common.ErrorHandler
import net.timedoor.newandroidtraining.data.model.profile.ProfileDeleteData

class DeleteAccountPresenter(private val view: DeleteAccountContract.View) : DeleteAccountContract.Presenter {
    private val handler = DeleteAccountHandler()
    private val accessToken: String

    init {
        // TODO() prepare user access token
        accessToken = ""
    }

    override fun deleteAccount() {
        handler.deleteAccount(accessToken)
            ?.doSubscribe(object : ErrorHandler<ProfileDeleteData>(view) {
                override fun onNext(t: ProfileDeleteData) {
                    if (t.status == 1) {
                        view.deleteAccountSuccess(t)
                    } else {
                        view.showError(t.error?.errorTitle ?: "", t.error?.errorMessage ?: "")
                    }
                }
            })
    }
}
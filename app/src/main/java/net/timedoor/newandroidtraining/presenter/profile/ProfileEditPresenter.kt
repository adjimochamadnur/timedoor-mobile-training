package net.timedoor.newandroidtraining.presenter.profile

import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.data.handler.profile.ProfileEditHandler

class ProfileEditPresenter(private val view: ProfileEditContract.View) : ProfileEditContract.Presenter {
    private val handler = ProfileEditHandler()
    private val accessToken: String

    init {
        accessToken = ""
    }

    override fun editProfile(
        nickName: String?,
        birthday: String?,
        residence: String?,
        gender: Int,
        job: Int,
        personality: Int,
        hobby: String?,
        aboutMe: String?,
        imageId: Int,
        languange: String?
    ) {
        val hashMap = hashMapOf<String, Any>()
        nickName?.let { hashMap[Constants.REQUEST_NAME_NICKNAME] = it }
        birthday?.let { hashMap[Constants.REQUEST_NAME_BIRTHDAY] = it }
        residence?.let { hashMap[Constants.REQUEST_NAME_RESIDENCE] = it }
        gender.takeIf { it != 0 }?.let { hashMap[Constants.REQUEST_NAME_GENDER] = it }
        job.takeIf { it != 0 }?.let { hashMap[Constants.REQUEST_NAME_JOB] = it }
        personality.takeIf { it != 0 }?.let { hashMap[Constants.REQUEST_NAME_PERSONALITY] = it }
        hobby?.let { hashMap[Constants.REQUEST_NAME_HOBBY] = it }
        aboutMe?.let { hashMap[Constants.REQUEST_NAME_ABOUT_ME] = it }
        imageId.takeIf { it != 0 }?.let { hashMap[Constants.REQUEST_NAME_IMAGE_ID] = it }
        languange?.let { hashMap[Constants.REQUEST_NAME_LANGUAGE] = it }

        postEditedProfile(hashMap)
    }

    private fun postEditedProfile(hashMap: HashMap<String, Any>) {
        TODO("Not yet implemented")
    }
}
package net.timedoor.newandroidtraining.presenter.login

import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.common.doSubscribe
import net.timedoor.newandroidtraining.data.handler.common.ErrorHandler
import net.timedoor.newandroidtraining.data.handler.login.LoginHandler
import net.timedoor.newandroidtraining.data.model.login.LoginData

class LoginPresenter(private val view: LoginContract.View) : LoginContract.Presenter {
    private val handler = LoginHandler()

    override fun login(email: String, password: String, language: String) {
        val hashMap = hashMapOf<String, String>()
        hashMap[Constants.REQUEST_NAME_LOGIN_ID] = email
        hashMap[Constants.REQUEST_NAME_PASSWORD] = password
        hashMap[Constants.REQUEST_NAME_LANGUAGE] = language

        handler.login(hashMap)
            ?.doSubscribe(object : ErrorHandler<LoginData>(view) {
                override fun onNext(t: LoginData) {
                    if (t.status == 1) {
                        saveUserData(t)
                        view.loginResponse(t)
                    } else {
                        view.showError(t.error?.errorTitle ?: "", t.error?.errorMessage ?: "")
                    }
                }
            })
    }

    private fun saveUserData(data: LoginData) {

    }
}
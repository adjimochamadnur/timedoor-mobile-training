package net.timedoor.newandroidtraining.presenter.account

import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.data.model.profile.ProfileDeleteData
import net.timedoor.newandroidtraining.presenter.common.BaseContract
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DeleteAccountContract {
    interface View : BaseContract.View {
        fun deleteAccountSuccess(data: ProfileDeleteData)
    }

    interface Presenter {
        fun deleteAccount()
    }

    interface Service {
        @GET("{ctrl}/{action}")
        fun deleteAccount(
            @Path("ctrl") ctrl: String,
            @Path("action") action: String,
            @Query(Constants.REQUEST_NAME_ACCESS_TOKEN) accessToken: String
        ) : Observable<ProfileDeleteData>
    }
}
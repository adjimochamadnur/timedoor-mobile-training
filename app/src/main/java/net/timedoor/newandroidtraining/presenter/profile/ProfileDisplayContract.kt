package net.timedoor.newandroidtraining.presenter.profile

import io.reactivex.Observable
import net.timedoor.newandroidtraining.data.model.profile.ProfileDisplayData
import net.timedoor.newandroidtraining.presenter.common.BaseContract
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface ProfileDisplayContract {
    interface View : BaseContract.View {
        fun profileDisplayFetched(data: ProfileDisplayData)
    }

    interface Presenter {
        fun fetchProfileDisplay()
        fun fetchProfileDisplay(userId: Int)
    }

    interface Service {
        @GET("{ctrl}/{action}")
        fun fetchProfile(
            @Path("ctrl") ctrl: String,
            @Path("action") action: String,
            @QueryMap hashMap: HashMap<String, Any>
        ) : Observable<ProfileDisplayData>
    }
}
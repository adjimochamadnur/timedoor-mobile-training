package net.timedoor.newandroidtraining.presenter.profile_feed

import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.common.doSubscribe
import net.timedoor.newandroidtraining.data.handler.common.ErrorHandler
import net.timedoor.newandroidtraining.data.handler.profile_feed.ProfileFeedHandler
import net.timedoor.newandroidtraining.data.model.profile_feed.ProfileFeedData

class ProfileFeedPresenter(private val view: ProfileFeedContract.View) : ProfileFeedContract.Presenter {
    private val handler = ProfileFeedHandler()
    private val accessToken: String
    private var lastLoginTime: String? = null

    init {
        accessToken = ""
    }

    override fun fetchProfileFeed() {
        val hashMap = hashMapOf<String, String>()
        hashMap[Constants.REQUEST_NAME_ACCESS_TOKEN] = accessToken
        lastLoginTime?.let { hashMap[Constants.REQUEST_NAME_LAST_LOGIN_TIME] = it }

        handler.fetchProfileFeed(hashMap)
            ?.doSubscribe(object : ErrorHandler<ProfileFeedData>(view) {
                override fun onNext(t: ProfileFeedData) {
                    if (t.status == 1) {
                        this@ProfileFeedPresenter.lastLoginTime = t.lastLoginTime
                        view.feedFetched(t.items!!)
                    } else {
                        view.showError(t.error?.errorTitle ?: "", t.error?.errorMessage ?: "")
                    }
                }
            })
    }
}
package net.timedoor.newandroidtraining.view.activity.common

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import net.timedoor.newandroidtraining.R
import net.timedoor.newandroidtraining.data.model.common.getTabList
import net.timedoor.newandroidtraining.view.adapter.my_page.MainTabAdapter
import kotlinx.android.synthetic.main.activity_main_tab.*

class MainTabActivity : BaseActivity() {
    private lateinit var mainTabAdapter: MainTabAdapter
    private val tabSelectedListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabReselected(tab: TabLayout.Tab?) {}
        override fun onTabSelected(tab: TabLayout.Tab?) {}

        override fun onTabUnselected(tab: TabLayout.Tab?) {
            if (this@MainTabActivity::mainTabAdapter.isInitialized) {
                tab?.apply {
                    val fragment = mainTabAdapter.getItem(position)
                    fragment.onDestroy()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_tab)

        initTabLayout()
    }

    private fun initTabLayout() {
        val tabList = getTabList()
        mainTabAdapter = MainTabAdapter(supportFragmentManager, tabList)

        view_pager?.apply {
            adapter = mainTabAdapter
        }

        tab_layout?.apply {
            setupWithViewPager(view_pager)
            addOnTabSelectedListener(tabSelectedListener)

            for ((i, tabItem) in tabList.withIndex()) {
                getTabAt(i)?.setIcon(tabItem.icon)
            }
        }
    }
}
package net.timedoor.newandroidtraining.view.activity.login

import android.content.Intent
import android.os.Bundle
import net.timedoor.newandroidtraining.R
import net.timedoor.newandroidtraining.data.model.login.LoginData
import net.timedoor.newandroidtraining.presenter.login.LoginContract
import net.timedoor.newandroidtraining.presenter.login.LoginPresenter
import net.timedoor.newandroidtraining.view.activity.common.BaseActivity
import net.timedoor.newandroidtraining.view.activity.common.MainTabActivity
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*

class LoginActivity : BaseActivity(), LoginContract.View {
    private val presenter: LoginContract.Presenter = LoginPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setToolbar()

        login_button?.setOnClickListener {
            if (validate()) {
                showProgressDialog()
                presenter.login(login_email?.editText?.text.toString(), login_password?.editText?.text.toString(), deviceLanguage)
            }
        }
    }

    private val deviceLanguage: String = Locale.getDefault().language.toString()

    private fun validate() : Boolean {
        //TODO() make validation with kotlin extension function for email and password
        return false
    }

    override fun loginResponse(data: LoginData) {
        hideProgressDialog()
        startActivity(Intent(this, MainTabActivity::class.java))
        finishAffinity()
    }
}
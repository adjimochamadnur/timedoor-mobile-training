package net.timedoor.newandroidtraining.view.fragment.profile_feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import net.timedoor.newandroidtraining.R
import net.timedoor.newandroidtraining.common.showAlert
import net.timedoor.newandroidtraining.data.model.profile_feed.ProfileFeedItem
import net.timedoor.newandroidtraining.presenter.profile_feed.ProfileFeedContract
import net.timedoor.newandroidtraining.presenter.profile_feed.ProfileFeedPresenter
import net.timedoor.newandroidtraining.view.activity.common.MainTabActivity

class ProfileFeedFragment : Fragment(), ProfileFeedContract.View {
    private lateinit var mainTabActivity: MainTabActivity
    private val presenter: ProfileFeedContract.Presenter = ProfileFeedPresenter(this)

    // region Lifecycle
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile_feed, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView(view)
    }

    override fun onResume() {
        super.onResume()
        presenter.fetchProfileFeed()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
    // endregion

    // region PrivateFunction
    private fun initRecyclerView(view: View) {
        // TODO() initialize recycler view and set adapter
    }
    // endregion

    // region Extension ProfileFeed
    override fun feedFetched(data: List<ProfileFeedItem>) {
        TODO("Not yet implemented")
    }

    override fun showError(title: String, message: String) {
        mainTabActivity.hideProgressDialog()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        activity?.showAlert(title, message, positiveListener = { dialogInterface, _ ->
            dialogInterface.dismiss()
        })?.show()
    }
    // endregion
}
package net.timedoor.newandroidtraining.view.fragment.my_page

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import net.timedoor.newandroidtraining.R
import net.timedoor.newandroidtraining.view.adapter.my_page.MyPageAdapter
import kotlinx.android.synthetic.main.fragment_my_page.view.*
import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.common.showAlert
import net.timedoor.newandroidtraining.data.model.profile.ProfileDeleteData
import net.timedoor.newandroidtraining.data.model.profile.ProfileDisplayData
import net.timedoor.newandroidtraining.data.model.profile.ProfileEditData
import net.timedoor.newandroidtraining.presenter.account.DeleteAccountContract
import net.timedoor.newandroidtraining.presenter.media.MediaUploadContract
import net.timedoor.newandroidtraining.presenter.profile.ProfileDisplayContract
import net.timedoor.newandroidtraining.presenter.profile.ProfileEditContract
import net.timedoor.newandroidtraining.view.activity.common.MainTabActivity
import net.timedoor.newandroidtraining.view.activity.common.WebViewActivity
import net.timedoor.newandroidtraining.view.activity.profile.ProfileEditActivity

class MyPageFragment : Fragment(), ProfileDisplayContract.View, ProfileEditContract.View, DeleteAccountContract.View, MediaUploadContract.View {
    private lateinit var recyclerAdapter: MyPageAdapter
    private lateinit var settingsAdapter: MyPageAdapter
    private lateinit var mainTabActivity: MainTabActivity

    // region Lifecycle
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_page, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler(view)

        view.edit_profile_button?.setOnClickListener {
            startActivity(Intent(context, ProfileEditActivity::class.java))
        }

        view.my_page_image?.setOnClickListener {
            // TODO() check permission
            if (checkPermission()) {
                showBottomSheet()
            } else {
                askPermission()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mainTabActivity = activity as MainTabActivity
        mainTabActivity.showProgressDialog()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_my_page, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.logout -> {}
        }
        return super.onOptionsItemSelected(item)
    }
    // endregion

    // region Private Function
    private fun initRecycler(view: View) {
        recyclerAdapter = MyPageAdapter()
        settingsAdapter = MyPageAdapter(true) {
            when (it) {
                0 -> {
                    with (Intent(context, WebViewActivity::class.java)) {
                        putExtra(Constants.INTENT_LINK, Constants.WEB_INFO_PAGE_TERMS_SERVICE)
                        startActivity(this)
                    }
                }
                1 -> {
                    // TODO() show yes/ no alert dialog. If yes, delete account. If no delete dialog
                }
            }
        }

        settingsAdapter.list = listOf(getString(R.string.term_and_condition), getString(R.string.delete_account_button))

        view.my_page_recycle?.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = recyclerAdapter
        }

        view.my_page_setting?.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = settingsAdapter
        }
    }

    private fun showBottomSheet() {

    }

    private fun checkPermission() : Boolean {
        return false
    }

    private fun askPermission() {

    }
    // endregion

    // region Extension Media Upload
    override fun uploadImageSuccess() {
        TODO("Not yet implemented")
    }

    override fun uploadVideoSuccess() {
        TODO("Not yet implemented")
    }
    // endregion

    //region Extension ProfileDisplay
    override fun profileDisplayFetched(data: ProfileDisplayData) {
        TODO("Not yet implemented")
    }
    // endregion

    // region Extension ProfileEdit
    override fun successEditProfile(data: ProfileEditData) {
        TODO("Not yet implemented")
    }
    // endregion

    // region Extension DeleteAccount
    override fun deleteAccountSuccess(data: ProfileDeleteData) {
        TODO("Not yet implemented")
    }
    // endregion

    // region Extension BaseContract
    override fun showError(title: String, message: String) {
        mainTabActivity.hideProgressDialog()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        activity?.showAlert(title, message, positiveListener = { dialogInterface, _ ->
            dialogInterface.dismiss()
        })?.show()
    }
    // endregion
}
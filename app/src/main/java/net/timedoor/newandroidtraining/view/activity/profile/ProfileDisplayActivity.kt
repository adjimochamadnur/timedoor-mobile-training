package net.timedoor.newandroidtraining.view.activity.profile

import android.os.Bundle
import net.timedoor.newandroidtraining.R
import net.timedoor.newandroidtraining.data.model.profile.ProfileDisplayData
import net.timedoor.newandroidtraining.presenter.profile.ProfileDisplayContract
import net.timedoor.newandroidtraining.view.activity.common.BaseActivity

class ProfileDisplayActivity : BaseActivity(), ProfileDisplayContract.View {

    // region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_display)
        setToolbar()

        // TODO() set click listener for button message to talk activity
    }

    // endregion

    // region Private Function
    private fun initRecyclerView() {
        // TODO() use recycler view to display user profile
    }
    // endregion

    // region Extension ProfileDisplay
    override fun profileDisplayFetched(data: ProfileDisplayData) {
        TODO("Not yet implemented")
    }
    // endregion
}
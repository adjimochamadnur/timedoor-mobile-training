package net.timedoor.newandroidtraining.view.activity.register

import android.content.Intent
import android.os.Bundle
import android.security.identity.ResultData
import net.timedoor.newandroidtraining.R
import net.timedoor.newandroidtraining.presenter.register.RegisterContract
import net.timedoor.newandroidtraining.presenter.register.RegisterPresenter
import net.timedoor.newandroidtraining.view.activity.common.BaseActivity
import net.timedoor.newandroidtraining.view.activity.common.MainTabActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : BaseActivity(), RegisterContract.View {
    private val presenter: RegisterContract.Presenter = RegisterPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        setToolbar(getString(R.string.register_toolbar_title))

        register_button?.setOnClickListener {
            if (validate()) {
                showProgressDialog()
                presenter.register(
                    register_username?.editText?.text.toString(),
                    register_email?.editText?.text.toString(),
                    register_password?.editText?.text.toString(),
                    deviceLanguage
                )
            }
        }
    }

    private val deviceLanguage: String = java.util.Locale.getDefault().language.toString()

    private fun validate() : Boolean {
        //TODO() make validation with kotlin extension function for email and password
        return false
    }

    override fun registerSuccess(data: ResultData) {
        hideProgressDialog()
        startActivity(Intent(this, MainTabActivity::class.java))
        finishAffinity()
    }
}
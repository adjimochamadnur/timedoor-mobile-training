package net.timedoor.newandroidtraining.view.activity.talk

import android.os.Bundle
import net.timedoor.newandroidtraining.R
import net.timedoor.newandroidtraining.data.model.talk.SendMessageData
import net.timedoor.newandroidtraining.data.model.talk.TalkItem
import net.timedoor.newandroidtraining.presenter.media.MediaUploadContract
import net.timedoor.newandroidtraining.presenter.talk.TalkContract
import net.timedoor.newandroidtraining.view.activity.common.BaseActivity

class TalkActivity : BaseActivity(), MediaUploadContract.View, TalkContract.View {

    // region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_talk)
        setToolbar()
    }
    // endregion

    // region Private Function
    private fun initRecycler() {
        // TODO() initialize recycler
    }

    private fun showBottomSheet() {
        // TODO() Show bottom sheet
    }
    // endregion

    // region Extension Talk
    override fun talkFetched(data: List<TalkItem>) {
        TODO("Not yet implemented")
    }

    override fun sendMessageSuccess(data: SendMessageData) {
        TODO("Not yet implemented")
    }
    // endregion

    // region Extension Media
    override fun uploadImageSuccess() {
        TODO("Not yet implemented")
    }

    override fun uploadVideoSuccess() {
        TODO("Not yet implemented")
    }
    // endregion
}
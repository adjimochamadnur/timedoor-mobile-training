package net.timedoor.newandroidtraining.view.activity.common

import android.app.Dialog
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import net.timedoor.newandroidtraining.R
import net.timedoor.newandroidtraining.common.showAlert
import net.timedoor.newandroidtraining.presenter.common.BaseContract
import kotlinx.android.synthetic.main.fragment_toolbar.*

open class BaseActivity : AppCompatActivity(), BaseContract.View {

    fun setToolbar(title: String? = null) {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            title?.let { setTitle(it) }
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    private  var dialog: Dialog? = null

    fun showProgressDialog() {
        if (dialog == null) {
            dialog = Dialog(this)
            val view = LayoutInflater.from(this).inflate(R.layout.fragment_progress_bar, null)
            dialog?.run {
                dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                window?.setBackgroundDrawableResource(android.R.color.transparent)
                setCancelable(false)
                setCanceledOnTouchOutside(false)
                setContentView(view)
            }
        }
        dialog!!.show()
    }

    fun hideProgressDialog() {
        dialog?.dismiss()
        dialog = null
    }

    override fun showError(title: String, message: String) {
        hideProgressDialog()
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        showAlert(title, message, positiveListener = { dialogInterface, _ ->
            dialogInterface.dismiss()
        }).show()
    }
}
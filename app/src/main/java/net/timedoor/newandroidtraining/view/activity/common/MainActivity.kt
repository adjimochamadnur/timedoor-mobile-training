package net.timedoor.newandroidtraining.view.activity.common

import android.content.Intent
import android.os.Bundle
import net.timedoor.newandroidtraining.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (checkIfLogin()) {
            val intent = Intent(this, MainTabActivity::class.java)
            startActivity(intent)
        }

        /* add null safety after net
        * linking net directly without findviewbyid or any injection can return null
        * if activity no longer showing or fragment no longer attach to activity
         */
        top_login?.setOnClickListener {
        }

        top_register?.setOnClickListener {
        }
    }

    private fun checkIfLogin() : Boolean {
        // TODO() check if user already login or not
        return false
    }

}
package net.timedoor.newandroidtraining.view.activity.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import net.timedoor.newandroidtraining.R

class WebViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
    }
}
package net.timedoor.newandroidtraining.view.activity.profile

import android.os.Bundle
import net.timedoor.newandroidtraining.R
import net.timedoor.newandroidtraining.data.model.profile.ProfileDisplayData
import net.timedoor.newandroidtraining.data.model.profile.ProfileEditData
import net.timedoor.newandroidtraining.presenter.profile.ProfileDisplayContract
import net.timedoor.newandroidtraining.presenter.profile.ProfileDisplayPresenter
import net.timedoor.newandroidtraining.presenter.profile.ProfileEditContract
import net.timedoor.newandroidtraining.presenter.profile.ProfileEditPresenter
import net.timedoor.newandroidtraining.view.activity.common.BaseActivity

class ProfileEditActivity : BaseActivity(), ProfileEditContract.View, ProfileDisplayContract.View {
    private val profilePresenter: ProfileDisplayContract.Presenter = ProfileDisplayPresenter(this)
    private val presenter: ProfileEditContract.Presenter = ProfileEditPresenter(this)

    // region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_edit)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        // TODO() Check if user change data before go back to previous activity
    }
    // endregion

    // region Private Function
    private fun initPickerData() {
        // TODO() Get data for picker from string-array in profile_item.xml
    }
    // endregion

    // region Extension ProfileDisplay
    override fun profileDisplayFetched(data: ProfileDisplayData) {
        TODO("Not yet implemented")
    }
    // endregion

    // region Extension ProfileEdit
    override fun successEditProfile(data: ProfileEditData) {
        TODO("Not yet implemented")
    }
    // endregion
}
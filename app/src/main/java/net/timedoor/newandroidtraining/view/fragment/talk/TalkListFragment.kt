package net.timedoor.newandroidtraining.view.fragment.talk

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_talk_list.*
import net.timedoor.newandroidtraining.R
import net.timedoor.newandroidtraining.common.showAlert
import net.timedoor.newandroidtraining.data.model.talk.DeleteTalkData
import net.timedoor.newandroidtraining.data.model.talk.TalkListData
import net.timedoor.newandroidtraining.presenter.talk.TalkListContract
import net.timedoor.newandroidtraining.presenter.talk.TalkListPresenter
import net.timedoor.newandroidtraining.view.activity.common.MainTabActivity
import net.timedoor.newandroidtraining.view.adapter.talk.TalkListAdapter

class TalkListFragment : Fragment(), TalkListContract.View {
    private val presenter: TalkListContract.Presenter = TalkListPresenter(this)
    private lateinit var mainTabActivity: MainTabActivity
    private lateinit var adapter: TalkListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_talk_list, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainTabActivity = activity as MainTabActivity

        talk_list_delete?.setOnClickListener {
            if (this::adapter.isInitialized) {
                adapter.talkIds
                    .takeIf { it.isNotBlank() }
                    ?.let {
                        mainTabActivity.showProgressDialog()
                        presenter.deleteTalk(it)
                    }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.fetchTalk()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_talk_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.edit -> {}
            R.id.cancel -> {}
        }
        return super.onOptionsItemSelected(item)
    }
    // endregion

    // region Private Function
    private fun initRecycler() {
        // TODO() initialize recycler and adapter
    }

    private fun setDeleteButtonVisibility(isShow: Boolean) {
        talk_list_delete?.visibility = if (isShow) View.VISIBLE else View.GONE
    }
    // endregion

    // region Extension TalkList
    override fun talkListFetched(data: TalkListData) {
        TODO("Not yet implemented")
    }

    override fun deleteSuccess(data: DeleteTalkData, talkIds: String) {
        TODO("Not yet implemented")
    }

    override fun showError(title: String, message: String) {
        mainTabActivity.hideProgressDialog()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        activity?.showAlert(title, message, positiveListener = { dialogInterface, _ ->
            dialogInterface.dismiss()
        })?.show()
    }
    // endregion
}
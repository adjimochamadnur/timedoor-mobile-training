package net.timedoor.newandroidtraining.view.adapter.my_page

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import net.timedoor.newandroidtraining.R
import kotlinx.android.synthetic.main.layout_recycler_my_page.view.*

class MyPageAdapter(
    private val isNextButtonVisible: Boolean = false,
    private val listener: ((Int) -> Unit)? = null
) : RecyclerView.Adapter<MyPageAdapter.ViewHolder>() {
    var list: List<String>? = null

    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val title = item.title
        val nextButton = item.next_image

        fun bind(listener: ((Int) -> Unit)?) = with(itemView) {
            setOnClickListener { listener?.invoke(adapterPosition) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_recycler_my_page, parent)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = list?.count() ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        list?.get(position)?.let {
            holder.title.text = it
            holder.nextButton.visibility = if (isNextButtonVisible) View.VISIBLE else View.GONE
        }

        holder.bind(listener)
        holder.setIsRecyclable(false)
    }
}
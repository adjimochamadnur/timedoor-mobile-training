package net.timedoor.newandroidtraining.common

import java.util.regex.Pattern

/**
 * Created by ooyama on 2017/05/24.
 */

object Constants {

    const val WEB_INFO_PAGE_TERMS_SERVICE = "https://s3-ap-northeast-1.amazonaws.com/app-lesson-media/tos.html"

    val EMAIL_PATTERN = Pattern.compile("^[a-zA-Z0-9\\._\\-\\+]+@[a-zA-Z0-9_\\-]+\\.[a-zA-Z\\.]+[a-zA-Z]$")
    val PASSWORD_PATTERN = Pattern.compile("[^a-zA-Z0-9._-]")

    // NOTE:通常はドメインを指定してください。
    val BASE_URL = "https://terraresta.com/app/api/"

    // APIコントローラ名称
    const val API_CTRL_NAME_SIGN_UP = "SignUpCtrl"
    const val API_CTRL_NAME_LOGIN = "LoginCtrl"
    const val API_CTRL_NAME_PROFILE_FEED = "ProfileFeedCtrl"
    const val API_CTRL_NAME_PROFILE = "ProfileCtrl"
    const val API_CTRL_NAME_TALK = "TalkCtrl"
    const val API_CTRL_NAME_MEDIA = "MediaCtrl"
    const val API_CTRL_NAME_ACCOUNT = "AccountCtrl"

    // APIアクション名称
    const val API_ACTION_NAME_SIGN_UP = "SignUp"
    const val API_ACTION_NAME_LOGIN = "Login"
    const val API_ACTION_NAME_PROFILE_FEED = "ProfileFeed"
    const val API_ACTION_NAME_PROFILE_DISPLAY = "ProfileDisplay"
    const val API_ACTION_NAME_TALK_LIST = "TalkList"
    const val API_ACTION_NAME_TALK = "Talk"
    const val API_ACTION_NAME_SEND_MESSAGE = "SendMessage"
    const val API_ACTION_NAME_IMAGE_UPLOAD = "ImageUpload"
    const val API_ACTION_NAME_VIDEO_UPLOAD = "VideoUpload"
    const val API_ACTION_NAME_PROFILE_EDIT = "ProfileEdit"
    const val API_ACTION_NAME_DELETE_ACCOUNT = "DeleteAccount"
    const val API_ACTION_NAME_DELETE_TALK_LIST = "Delete"

    // APIリクエストパラメータ
    const val REQUEST_NAME_LOGIN_ID = "login_id"
    const val REQUEST_NAME_PASSWORD = "password"
    const val REQUEST_NAME_NICKNAME = "nickname"
    const val REQUEST_NAME_LANGUAGE = "language"
    const val REQUEST_NAME_ACCESS_TOKEN = "access_token"
    const val REQUEST_NAME_LAST_LOGIN_TIME = "last_login_time"
    const val REQUEST_NAME_USER_ID = "user_id"
    const val REQUEST_NAME_LAST_UPDATE_TIME = "last_update_time"
    const val REQUEST_NAME_TO_USER_ID = "to_user_id"
    const val REQUEST_NAME_BORDER_MESSAGE_ID = "border_message_id"
    const val REQUEST_NAME_HOW_TO_REQUEST = "how_to_request"
    const val REQUEST_NAME_MESSAGE = "message"
    const val REQUEST_NAME_IMAGE_ID = "image_id"
    const val REQUEST_NAME_VIDEO_ID = "video_id"
    const val REQUEST_NAME_DATA = "data"
    const val REQUEST_NAME_BIRTHDAY = "birthday"
    const val REQUEST_NAME_RESIDENCE = "residence"
    const val REQUEST_NAME_GENDER = "gender"
    const val REQUEST_NAME_JOB = "job"
    const val REQUEST_NAME_PERSONALITY = "personality"
    const val REQUEST_NAME_HOBBY = "hobby"
    const val REQUEST_NAME_ABOUT_ME = "about_me"
    const val REQUEST_LOCATION = "location"
    const val REQUEST_NAME_TALK_IDS = "talk_ids"

    const val SHARED_PREFERENCES = "shared_preference"
    const val INTENT_LINK = "link_url"
}

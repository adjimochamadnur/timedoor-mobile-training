package net.timedoor.newandroidtraining.common

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import net.timedoor.newandroidtraining.R
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T: Any> Observable<T>.doSubscribe(observer: Observer<in T>) {
    this.subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .retry(3)
        .subscribe(observer)
}

fun Context.showAlert(
    title: String? = null,
    message: String,
    okTitle: String = getString(R.string.ok),
    positiveListener: ((DialogInterface, Int) -> Unit),
    cancelTitle: String? = null,
    negativeListener: ((DialogInterface, Int) -> Unit)? = null
) : AlertDialog.Builder {

    return AlertDialog.Builder(this).apply {
        title?.let { this.setTitle(it) }
        setMessage(message)
        setPositiveButton(okTitle, positiveListener)

        if (cancelTitle != null && negativeListener != null) {
            setNegativeButton(cancelTitle, negativeListener)
        }
    }
}
package net.timedoor.newandroidtraining.common

import android.content.Context
import android.content.SharedPreferences

class Preferences(context: Context) {
    private var sharedPreferences: SharedPreferences
    private var editor: SharedPreferences.Editor

    init {
        sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
    }

    fun userLoggedOut() {
        editor.clear()
    }
}

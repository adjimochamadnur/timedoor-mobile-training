package net.timedoor.newandroidtraining.common

import android.app.Application
import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration

class GlobalClass : Application(){

    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
    }

    private fun initRealm() {
        // TODO() Init Realm
        Realm.init(context)
        val realmConfiguration = RealmConfiguration.Builder()
            .build()

        Realm.setDefaultConfiguration(realmConfiguration)
    }
}
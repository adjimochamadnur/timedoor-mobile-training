package net.timedoor.newandroidtraining.common

import net.timedoor.newandroidtraining.BuildConfig
import net.timedoor.newandroidtraining.data.handler.common.BaseHandler
import net.timedoor.newandroidtraining.data.model.common.APIError
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Response
import java.io.IOException

object Utilities {

    /**
     * API_URL取得
     * Acquisition of API_URL
     *
     * @return url
     */
    //return String.format("http://%s/ms4/mb/", Constants.SERVER_DOMAIN);
    //return String.format("http://%s/ms/mb/", Constants.SERVER_DOMAIN);
    val apiUrl: String
        get() {
            if (BuildConfig.DEBUG) {
            } else {
            }
            return "https://www.googleapis.com/"
        }

    /**
     * API_URL取得(https用)
     * Acquisition of API_URL(for https)
     *
     * @return url
     */
    //return String.format("https://%s/ms4/mb/", Constants.SERVER_DOMAIN);
    //return String.format("https://%s/ms/mb/", Constants.SERVER_DOMAIN);
    val apiUrlForHttps: String
        get() {
            if (BuildConfig.DEBUG) {
            } else {
            }
            return "https://www.googleapis.com/"
        }

    /**
     * Emailの正規表現チェック
     *
     * @param email e-mail
     * @return true:OK,false:NG
     */
    fun emailCheck(email: String): Boolean {
        return !Constants.EMAIL_PATTERN.matcher(email).find()
    }

    /**
     * パスワードの正規表現チェック
     *
     * @param password パスワード
     * @return true:OK,false:NG
     */
    fun passwordCheck(password: String): Boolean {
        return !Constants.PASSWORD_PATTERN.matcher(password).find()
    }

    fun parseError(response: Response<*>): APIError {
        val converter: Converter<ResponseBody, APIError> = BaseHandler().getClient()
            .responseBodyConverter(APIError::class.java, arrayOfNulls<Annotation>(0))

        val error: APIError

        try {
            error = converter.convert(response.errorBody())!!
        } catch (e: IOException) {
            e.printStackTrace()
            return APIError()
        }
        return error
    }

}

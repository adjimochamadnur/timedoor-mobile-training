package net.timedoor.newandroidtraining.data.handler.profile

import io.reactivex.Observable
import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.data.handler.common.BaseHandler
import net.timedoor.newandroidtraining.data.model.profile.ProfileEditData
import net.timedoor.newandroidtraining.presenter.profile.ProfileEditContract

class ProfileEditHandler : BaseHandler(){
    private val request = getClient().create(ProfileEditContract.Service::class.java)

    fun editProfile(accessToken: String, hashMap: HashMap<String, Any>): Observable<ProfileEditData>? {
        return request.editProfile(Constants.API_CTRL_NAME_PROFILE, Constants.API_ACTION_NAME_PROFILE_EDIT, accessToken, hashMap)
    }
}
package net.timedoor.newandroidtraining.data.model.common

import androidx.fragment.app.Fragment

data class TabData(val icon: Int, val title: String, val fragment: Fragment)

fun getTabList() : List<TabData> {
    val tabList = listOf<TabData>()
    return tabList
}


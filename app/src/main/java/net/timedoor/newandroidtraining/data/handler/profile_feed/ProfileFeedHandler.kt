package net.timedoor.newandroidtraining.data.handler.profile_feed

import io.reactivex.Observable
import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.data.handler.common.BaseHandler
import net.timedoor.newandroidtraining.data.model.profile_feed.ProfileFeedData
import net.timedoor.newandroidtraining.presenter.profile_feed.ProfileFeedContract

class ProfileFeedHandler : BaseHandler() {
    private val request = getClient().create(ProfileFeedContract.Service::class.java)

    fun fetchProfileFeed(hashMap: HashMap<String, String>) : Observable<ProfileFeedData>? {
        return request.fetchProfileFeed(Constants.API_CTRL_NAME_PROFILE_FEED, Constants.API_ACTION_NAME_PROFILE_FEED, hashMap)
    }
}
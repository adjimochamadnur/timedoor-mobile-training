package net.timedoor.newandroidtraining.data.model.login

import net.timedoor.newandroidtraining.data.model.common.BaseResultData
import net.timedoor.newandroidtraining.data.model.common.ErrorData

class LoginData: BaseResultData() {
    var userId: Int =0
    var accessToken :String=""
    var status :Int=0
    var error : ErrorData?=null
}

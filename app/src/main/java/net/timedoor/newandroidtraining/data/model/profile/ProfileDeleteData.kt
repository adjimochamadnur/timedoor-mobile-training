package net.timedoor.newandroidtraining.data.model.profile

import net.timedoor.newandroidtraining.data.model.common.BaseResultData
import net.timedoor.newandroidtraining.data.model.common.ErrorData

class ProfileDeleteData : BaseResultData() {
    var status : Int = 0
    var error : ErrorData?= null
}
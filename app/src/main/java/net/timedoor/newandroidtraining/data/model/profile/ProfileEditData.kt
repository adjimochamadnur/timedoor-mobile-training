package net.timedoor.newandroidtraining.data.model.profile

import com.google.gson.annotations.Expose
import net.timedoor.newandroidtraining.data.model.common.ErrorData

class ProfileEditData {
    @Expose var status : Int=0
    @Expose var error : ErrorData?=null
}
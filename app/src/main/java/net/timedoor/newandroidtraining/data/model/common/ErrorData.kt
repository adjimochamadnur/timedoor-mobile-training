package net.timedoor.newandroidtraining.data.model.common

import com.google.gson.annotations.Expose

class ErrorData {
    @Expose
    val errorCode: Int = 0
    @Expose
    val errorTitle: String? = null
    @Expose
    val errorMessage: String? = null
}


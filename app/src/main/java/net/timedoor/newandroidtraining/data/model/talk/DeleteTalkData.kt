package net.timedoor.newandroidtraining.data.model.talk

import net.timedoor.newandroidtraining.data.model.common.BaseResultData
import net.timedoor.newandroidtraining.data.model.common.ErrorData

class DeleteTalkData : BaseResultData() {
    var status : Int = 0
    var error : ErrorData?= null
}

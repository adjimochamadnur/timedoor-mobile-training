package net.timedoor.newandroidtraining.data.model.media

import com.google.gson.annotations.SerializedName
import net.timedoor.newandroidtraining.data.model.common.BaseResultData
import net.timedoor.newandroidtraining.data.model.common.ErrorData

class ImageUploadData : BaseResultData() {

    var status : Int = 0

    @SerializedName("imageId") var imageId : Int = 0

    var error: ErrorData? = null
}

package net.timedoor.newandroidtraining.data.handler.talk

import io.reactivex.Observable
import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.data.handler.common.BaseHandler
import net.timedoor.newandroidtraining.data.model.talk.SendMessageData
import net.timedoor.newandroidtraining.data.model.talk.TalkData
import net.timedoor.newandroidtraining.presenter.talk.TalkContract

class TalkHandler : BaseHandler() {
    private val request = getClient().create(TalkContract.Service::class.java)

    fun fetchTalk(hashMap: HashMap<String, Any>) : Observable<TalkData>? {
        return request.fetchTalk(Constants.API_CTRL_NAME_TALK, Constants.API_ACTION_NAME_TALK, hashMap)
    }

    fun sendMessage(hashMap: HashMap<String, Any>) : Observable<SendMessageData>? {
        return request.sendMessage(Constants.API_CTRL_NAME_TALK, Constants.API_ACTION_NAME_SEND_MESSAGE, hashMap)
    }
}
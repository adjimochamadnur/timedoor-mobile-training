package net.timedoor.newandroidtraining.data.handler.account

import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.data.handler.common.BaseHandler
import net.timedoor.newandroidtraining.data.model.profile.ProfileDeleteData
import net.timedoor.newandroidtraining.presenter.account.DeleteAccountContract
import io.reactivex.Observable

class DeleteAccountHandler : BaseHandler() {
    private val request = getClient().create(DeleteAccountContract.Service::class.java)

    fun deleteAccount(accessToken: String) : Observable<ProfileDeleteData>? {
        return request.deleteAccount(Constants.API_CTRL_NAME_ACCOUNT, Constants.API_ACTION_NAME_DELETE_ACCOUNT, accessToken)
    }
    // this is new comment
}
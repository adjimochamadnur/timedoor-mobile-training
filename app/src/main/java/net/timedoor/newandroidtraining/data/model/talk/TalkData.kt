package net.timedoor.newandroidtraining.data.model.talk

import com.google.gson.annotations.Expose
import io.realm.annotations.PrimaryKey
import net.timedoor.newandroidtraining.data.model.common.BaseResultData
import net.timedoor.newandroidtraining.data.model.common.ErrorData

class TalkData : BaseResultData() {
    @Expose var status: Int = 0
    @Expose var items: List<TalkItem>? = null
    @Expose var error: ErrorData?=null
}

class TalkItem {
    @Expose var talkId: Int = 0
    @Expose var messageId: Int = 0
    @Expose var imageId: Int = 0
    @Expose var imageSize: String? = null
    @Expose var imageUrl: String? = null
    @Expose var userId: Int = 0
    @Expose var nickname: String? = null
    @Expose var message: String? = null
    @Expose var mediaId: Int = 0
    @Expose var mediaSize: String? = null
    @Expose var mediaUrl: String? = null
    @Expose var mediaType: Int = 0
    @Expose var time: String? = null
    @Expose var messageKind: Int = 0
}
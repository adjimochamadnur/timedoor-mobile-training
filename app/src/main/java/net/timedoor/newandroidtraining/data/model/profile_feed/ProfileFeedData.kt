package net.timedoor.newandroidtraining.data.model.profile_feed

import com.google.gson.annotations.Expose
import net.timedoor.newandroidtraining.data.model.common.BaseResultData
import net.timedoor.newandroidtraining.data.model.common.ErrorData

class ProfileFeedData : BaseResultData() {
    @Expose var status: Int = 0
    @Expose var lastLoginTime: String = ""
    @Expose var items: List<ProfileFeedItem>? = null
    @Expose var error: ErrorData? = null
}

class ProfileFeedItem {
    @Expose var userId: Int = 0
    @Expose var nickname: String = ""
    @Expose var imageId: Int = 0
    @Expose var imageSize: String? = null
    @Expose var imageUrl: String? = null
    @Expose var age: Int = 0
    @Expose var residence: String? = null
    @Expose var aboutMe: String? = null
}
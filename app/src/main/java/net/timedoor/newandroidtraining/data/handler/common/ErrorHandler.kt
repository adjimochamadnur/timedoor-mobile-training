package net.timedoor.newandroidtraining.data.handler.common

import android.util.Log
import net.timedoor.newandroidtraining.R
import net.timedoor.newandroidtraining.common.GlobalClass.Companion.context
import net.timedoor.newandroidtraining.common.Utilities
import net.timedoor.newandroidtraining.data.model.common.BaseResultData
import net.timedoor.newandroidtraining.presenter.common.BaseContract
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import java.io.IOException
import java.lang.ref.WeakReference
import java.net.SocketTimeoutException
import java.net.UnknownHostException


abstract class ErrorHandler<T : BaseResultData>(view: BaseContract.View) : Observer<T> {
    private val TAG = "APIError Handler"
    override fun onComplete() {}

    override fun onSubscribe(d: Disposable) {}

    private val weakReference: WeakReference<BaseContract.View> = WeakReference(view)
    override fun onError(e: Throwable) {
        val view = weakReference.get()
        when(e) {
            is HttpException -> {
                val error = e.response()?.let { Utilities.parseError(it) }
                Log.i(TAG, e.response()?.code().toString())
                when(e.response()?.code()) {
                    403 -> view?.showError(
                        context.getString(R.string.error),
                        context.getString(R.string.forbidden)
                    )
                    422 -> view?.showError(
                        context.getString(R.string.error), context.getString(
                            R.string.unprocessed_entity
                        )
                    )
                    else -> view?.showError(
                        error?.error?.errorTitle.toString(),
                        error?.error?.errorMessage.toString()
                    )
                }

            }
            is UnknownHostException -> {
                Log.i(TAG, "No connection")
                view?.showError(
                    context.getString(R.string.error),
                    context.getString(R.string.no_connection)
                )
            }
            is SocketTimeoutException -> {
                Log.i(TAG, "Timeout == ${e.cause}")
                view?.showError(
                    context.getString(R.string.error)
                    , context.getString(R.string.connection_timeout) + "==" + e.cause
                )
            }
            is IOException -> {
                Log.i(TAG, "IO Exception == ${e.localizedMessage}")
                view?.showError(
                    context.getString(R.string.error),
                    context.getString(R.string.io_exception) + "==" + e.localizedMessage
                )
            }
            else -> {
                Log.i(TAG, e.localizedMessage)
            }
        }
    }

}

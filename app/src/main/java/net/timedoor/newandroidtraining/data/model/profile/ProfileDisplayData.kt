package net.timedoor.newandroidtraining.data.model.profile

import com.google.gson.annotations.Expose
import net.timedoor.newandroidtraining.data.model.common.BaseResultData
import net.timedoor.newandroidtraining.data.model.common.ErrorData

class ProfileDisplayData : BaseResultData() {
    @Expose var status: Int = 0

    @Expose var userId = 0

    @Expose var nickname: String = ""

    @Expose var imageId: Int = 0

    @Expose var imageSize: String? = null

    @Expose var imageUrl: String? = null

    @Expose var gender: Int = 0

    @Expose var age: Int = 0

    @Expose var job: Int = 0

    @Expose var residence: String? = null

    @Expose var personality: Int = 0

    @Expose var hobby: String? = null

    @Expose var aboutMe: String? = null

    @Expose var userStatus: Int = 0

    @Expose var email: String? = null

    @Expose var password: String? = null

    @Expose var birthday: String? =null

    @Expose
    var error: ErrorData? = null

}

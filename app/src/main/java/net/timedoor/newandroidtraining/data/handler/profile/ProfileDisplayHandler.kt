package net.timedoor.newandroidtraining.data.handler.profile

import io.reactivex.Observable
import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.data.handler.common.BaseHandler
import net.timedoor.newandroidtraining.data.model.profile.ProfileDisplayData
import net.timedoor.newandroidtraining.presenter.profile.ProfileDisplayContract

class ProfileDisplayHandler : BaseHandler() {
    private val request = getClient().create(ProfileDisplayContract.Service::class.java)

    fun fetchProfile(hashMap: HashMap<String, Any>) : Observable<ProfileDisplayData>? {
        return request.fetchProfile(Constants.API_CTRL_NAME_PROFILE, Constants.API_ACTION_NAME_PROFILE_DISPLAY, hashMap)
    }
}
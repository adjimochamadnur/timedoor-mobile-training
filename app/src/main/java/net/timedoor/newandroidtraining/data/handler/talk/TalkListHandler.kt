package net.timedoor.newandroidtraining.data.handler.talk

import io.reactivex.Observable
import net.timedoor.newandroidtraining.common.Constants
import net.timedoor.newandroidtraining.data.handler.common.BaseHandler
import net.timedoor.newandroidtraining.data.model.talk.DeleteTalkData
import net.timedoor.newandroidtraining.data.model.talk.TalkListData
import net.timedoor.newandroidtraining.presenter.talk.TalkListContract

class TalkListHandler : BaseHandler() {
    private val request = getClient().create(TalkListContract.Service::class.java)

    fun fetchTalkList(hashMap: HashMap<String, String>) : Observable<TalkListData>? {
        return request.fetchTalkList(Constants.API_CTRL_NAME_TALK, Constants.API_ACTION_NAME_TALK_LIST, hashMap)
    }

    fun deleteTalkList(hashMap: HashMap<String, String>) : Observable<DeleteTalkData>? {
        return request.deleteTalks(Constants.API_CTRL_NAME_TALK, Constants.API_ACTION_NAME_DELETE_TALK_LIST, hashMap)
    }
}